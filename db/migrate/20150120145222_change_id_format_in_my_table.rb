class ChangeIdFormatInMyTable < ActiveRecord::Migration
  def up
    change_column :users, :user_id, :string
    rename_column :users, :user_id, :email
  end

  def down
    change_column :users, :user_id, :integer
  end
end
