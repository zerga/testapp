class User < ActiveRecord::Base
	devise :omniauthable, :omniauth_providers => [:facebook]

	def self.from_omniauth(auth)
      user = User.first_or_create(provider: auth.provider, uid: auth.uid)
      user.update(name: auth.info.name, email: auth.info.email, acces_token: auth.credentials.token)
      return user
  		#where(provider: auth.provider, uid: auth.uid).first_or_create do |user|
    	#user.email = auth.info.email
    	#user.name = auth.info.name
      #user.acces_token = auth.credentials.token
  		#end
	end

	def self.new_with_session(params, session)
    super.tap do |user|
      if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
        user.email = data["email"]
        #user.email = omniauth["extra"]["user_hash"]["email"]
      end
    end
  end

end
